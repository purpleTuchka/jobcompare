﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareJob
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string pathSource = Environment.CurrentDirectory + "/../../Source.txt";                 //файлы в корневой папке там где и Program.cs
            string pathTarget = Environment.CurrentDirectory + "/../../Target.txt";
            string pathResult = Environment.CurrentDirectory + "/../../Result.txt";

            string result = null;

            StreamReader sour = new StreamReader(pathSource);
            string line=null;
            StreamReader targ = new StreamReader(pathTarget);
            string line2=null;
            bool end = true;

            while (end)
            {
                line = sour.ReadLine();
                line2 = targ.ReadLine();

                if (line != null && line2 != null)                               //проверка на перенос строк
                {
                    if (line == line2)
                    {
                        result += line + '\n';
                    }
                    else if (line == "" && line2 != "")
                    {
                        do
                        {
                            result += "[]\n";
                        } while ((line = sour.ReadLine()) == "");
                        result += Compare(line, line2) + '\n';

                    }
                    else if (line2 == "")
                    {
                        do
                        {
                            result += "()\n";
                        } while ((line2 = targ.ReadLine()) == "");
                        result += Compare(line, line2) + '\n';

                    }
                    else
                    {
                        result += Compare(line, line2) + '\n';
                    }

                }
                else
                {
                    if (line == null && line2 == null)
                    {
                        end = false;
                        break;
                    }
                    if (line == null && line2 != null)
                    {
                        result += "(" + line2 + ")" + '\n';
                    }
                    else if (line2 == null && line != null)
                    {
                        result += "[" + line + "]" + '\n';
                    }
                }

            }
            Console.WriteLine("Text wich will be written to file:");
            Console.WriteLine(result);
            string[] lineWords = result.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);               //запись в файл
            try
            {
                using (StreamWriter sw = new StreamWriter(pathResult, false, System.Text.Encoding.Default))
                {
                    foreach(string l in lineWords)
                    sw.WriteLine(l);
                }
                Console.WriteLine("Text has been saved in the file \"result\"");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            

        }
        static string Compare (string line1,string line2)
        {
            string result = null;
            if (line1 == line2)              //если строки равны
            {
                result += line1;
                return result;
            }
            else                                                                                                                    //если не равны
            {
                string[] lineWords = line1.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);         //разделяем обе строки с разных файлов на слова
                string[] lineWords2 = line2.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
                
                
                for (int i = 0; i < lineWords.Length; i++)
                {
                    if (i >= lineWords.Length || i >= lineWords2.Length)
                        break;
                    if (lineWords[i] == lineWords2[i])                     //если слова равны добавляем к результирующей строке
                    {
                        result +=lineWords[i]+" ";
                    }
                    else                                                         // если не равны
                    {
                        string strSource = lineWords[i];
                        string strTarget = lineWords2[i];
                        string strResult = null;

                        Console.WriteLine("Source: " + strSource);                          //Первое слово
                        Console.WriteLine("Target: " + strTarget);                          //Второе слово     
                        //Console.WriteLine("Compare%: " + Relev(strSource, strTarget));
                        int indexOfmiss = -1;
                        if (Relev(strSource, strTarget) > 30)          // сравнение двух слов по буквам, если больше 25% совпадений то проходит дальше
                        {
                            string substrSquare = null;
                            string substrRound = null;
                            int k = 0;
                            for (int j = 0; j < strSource.Length; j++)                              
                            {
                                bool found = false;
                                if (j >= strTarget.Length&&k>= strTarget.Length)
                                {
                                    strResult += "[" + strSource[j] + "]";
                                }
                                for (; k < strTarget.Length; k++)                        //цикл для поиска буквы первого слова входящую во второе слово, и запись букв до того как найдет такую же букву,
                                {                                                       // если не находит то эта буква первого слова 100% заносится в квадратные скобки и итератор переходит на индекс второй строки где буквы не совпали,
                                    if (strSource[j] == strTarget[k])                   //  а если находит то буквы которые были перед найденной заносятся в круглые скобки
                                    {
                                        if (substrSquare != null)
                                        {
                                            strResult += "[" + substrSquare + "]";
                                            substrSquare = null;
                                        }
                                        strResult += strSource[j];
                                        k++;
                                        break;
                                    }
                                    else if (strSource[j] != strTarget[k])
                                    {
                                        indexOfmiss++;
                                        while (strSource[j] != strTarget[k])
                                        {
                                            if (k < strTarget.Length && strSource[j] != strTarget[k])
                                            {
                                                substrRound += strTarget[k];
                                            }
                                            k++;
                                            if (k < strTarget.Length && strSource[j] == strTarget[k])
                                            {
                                                found = true;
                                                break;
                                            }

                                            if (k >= strTarget.Length && found == false)
                                            {
                                                substrSquare += strSource[j];
                                                if (k >= strTarget.Length&&j==strSource.Length-1)
                                                {
                                                    if (substrSquare != null)
                                                        strResult +="[" + substrSquare + "]";
                                                    if (substrRound != null)
                                                        strResult += "(" + substrRound + ")";
                                                }
                                                substrRound = null;
                                                break;
                                            }
                                        }
                                        
                                        if (found)
                                        {
                                            if (substrSquare != null)
                                                strResult += "[" + substrSquare + "]";
                                            if (substrRound != null)
                                                strResult += "(" + substrRound + ")";
                                            strResult += strSource[j];
                                            indexOfmiss = -1;
                                            k++;
                                            substrRound = null;
                                            substrSquare = null;
                                            break;
                                        }
                                        else
                                        {
                                            k = j - indexOfmiss;
                                            break;
                                        }



                                    }
                                }
                            }
                            if(k<strTarget.Length)
                            while(k<strTarget.Length)
                            {
                                strResult +="("+strTarget[k]+")";
                                k++;
                            }
                            result += strResult+' ';
                            Console.WriteLine("Result: " + strResult + '\n');

                        }
                        else
                        {
                            result += "[" + strSource + "] " + " (" + strTarget + ") ";
                        }
                    }
                }
                
            }

            return result;
        }

        static int Relev(string strSource, string strTarget) //функция для определения на сколько два слова скожи друг с дургом
        {
            int r = 0;
            int k = 0;
            for (int i = 0; i < strSource.Length; i++)
            {
                bool found = false;
                for (; k < strTarget.Length; k++)
                {
                    if (strSource[i] == strTarget[k])
                    {
                        r++;
                        found = true;
                        break;
                    }
                }
                if (found) k++;
                if (!found && k >= strTarget.Length) k = 0;
            }
            r = r * 100 / strSource.Length; 
            return r;
        }
        
    }
}